
<form method="get" class="searchform form-inline search_form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<?php

		$search_categories = get_categories( 
			array(
		        	'taxonomy' => 'game_types',
				    'orderby'  => 'name',
				    'order'    => 'ASC'
				)
		);
		
	?>
	<?php if(!empty($search_categories)): ?>	

		<div class="form-group mb-2 pb-2 choose_search_cat">
			
			<label class="form-label form-inline pr-2">
				<small>Search in:</small> 
			</label>

			<label class="form-radio form-inline">
			    <input type="radio" id="all" name="term" value="" <?php echo ( empty($_GET['term']) ? 'checked' : '' ); ?> >
			    <i class="form-icon"></i>
			    <small>All games</small>
			</label>

			<?php

				foreach( $search_categories as $category ) {
					echo '<label class="form-radio form-inline">
						    <input type="radio" id="' . $category->slug . '" name="term" value="' . $category->slug . '" ' . ( ( isset($_GET['term']) && $_GET['term'] === $category->slug ) ? 'checked' : '' ) . '>
						    <i class="form-icon"></i>
						    <small>' . ucfirst($category->name) . '</small>
						  </label>';
				} 

			?>
			
		</div>
	<?php endif; ?>
	<div class="input-group">
		<span class="input-group-addon">
			<span class="icon">
				<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 468.293 468.293" style="enable-background:new 0 0 468.293 468.293;" xml:space="preserve" width="25px" height="25px">
					<g>
						<path style="fill:#D5D6DB;" d="M337.881,71.84H108.229c-4.472,0-8.096,3.625-8.096,8.096c0,4.472,3.625,8.096,8.096,8.096h229.652
							c7.769,0,14.086,6.318,14.086,14.081s-6.318,14.081-14.086,14.081h-81.764c-16.582,0-30.07,13.488-30.07,30.07v12.122
							c0,4.472,3.625,8.096,8.096,8.096c4.472,0,8.096-3.625,8.096-8.096v-12.122c0-7.656,6.222-13.878,13.878-13.878h81.764
							c16.695,0,30.279-13.584,30.279-30.273S354.576,71.84,337.881,71.84z"/>
						<path style="fill:#D5D6DB;" d="M358.446,396.453h-248.6C49.18,396.453,0,347.273,0,286.607v0C0,225.94,49.18,176.76,109.846,176.76
							h248.6c60.666,0,109.846,49.18,109.846,109.846v0C468.293,347.273,419.113,396.453,358.446,396.453z"/>
					</g>
					<g>
						<circle style="fill:#EBF0F3;" cx="360.086" cy="286.595" r="85.104"/>
						<circle style="fill:#EBF0F3;" cx="108.232" cy="286.595" r="85.104"/>
					</g>
					<polygon style="fill:#27A2DB;" points="166.045,271.327 123.51,271.327 123.51,228.793 92.951,228.793 92.951,271.327 
						50.417,271.327 50.417,301.886 92.951,301.886 92.951,344.421 123.51,344.421 123.51,301.886 166.045,301.886 "/>
					<g>
						<circle style="fill:#44C4A1;" cx="360.086" cy="245.129" r="20.236"/>
						<circle style="fill:#44C4A1;" cx="360.086" cy="328.055" r="20.236"/>
					</g>
					<g>
						<circle style="fill:#1C75A1;" cx="401.539" cy="286.595" r="20.236"/>
						<circle style="fill:#1C75A1;" cx="318.564" cy="286.595" r="20.236"/>
					</g>
					<path style="fill:#3A556A;" d="M263.053,176.76h-57.814v-12.133c0-3.448,2.796-6.244,6.244-6.244h45.326
						c3.448,0,6.244,2.796,6.244,6.244V176.76z"/>
				</svg>
			</span>
		</span>

	  	<input type="text" class="form-input input-lg search_field" name="s" placeholder="<?php _e( 'Game search', 'adventurebeta2_theme' ); ?>" title="search" size="22" value="<?php echo ( !empty(get_search_query()) ? get_search_query() : ''); ?>">

	  	<button class="btn btn-primary btn-lg input-group-btn" type="submit" name="submit">
	  		<span class="icon icon-search"></span>
	  	</button>
	</div>
	<input type="hidden" name="taxonomy" value="game_types">
</form>