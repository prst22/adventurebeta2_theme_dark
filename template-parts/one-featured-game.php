<?php

	$term = get_term( get_the_ID() );
	$cur_time = time();
        
 ?>
<article <?php post_class( array('index_game', 'column', 'col-xs-12', 'col-sm-12', 'col-md-6', 'col-lg-6', 'col-xl-4', 'col-3') ); ?>>
	<div class="index_game__inner">
		<?php 
			$post_date = get_the_date( 'U', get_the_ID() );
			$bage = voxel_new_badge($post_date, $cur_time, get_the_ID());
			echo $bage;
			$views = get_post_meta(get_the_ID(), 'voxel_theme_game_views', TRUE);
		?>

			<?php if(has_post_thumbnail()): ?><!-- has thumb start -->
				<figure class="thumbnail_cnt">
					<span class="thumbnail_cnt__game_views" title="game views">
						<span class="icon">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" width="25px" height="25px">
								<g>
									<g>
										<circle style="fill:#FF583E;" cx="256" cy="256" r="256"/>
									</g>	
									<path style="fill:#F92814;" d="M409.332,197.866L102.665,314.134l194.548,194.548c108.162-17.507,193.599-102.774,211.366-210.848   L409.332,197.866z"/>
									<g>
										<path style="fill:#FFFFFF;" d="M256,135.514c-85.643,0-159.835,48.998-196.093,120.486    C96.165,327.488,170.357,376.486,256,376.486S415.835,327.488,452.093,256C415.835,184.512,341.643,135.514,256,135.514z"/>
									</g>
									<g>
										<path style="fill:#DCE1EB;" d="M256,135.514c-0.124,0-0.247,0.003-0.371,0.003v240.966c0.124,0,0.247,0.003,0.371,0.003    c85.643,0,159.835-48.998,196.094-120.486C415.835,184.512,341.643,135.514,256,135.514z"/>
									</g>
									<g>
										<circle style="fill:#80DBFF;" cx="256" cy="256" r="108.789"/>
									</g>
									<g>
										<path style="fill:#4AB8F7;" d="M256,147.211c-0.124,0-0.247,0.004-0.371,0.005v217.569c0.124,0,0.247,0.005,0.371,0.005    c60.082,0,108.789-48.706,108.789-108.789C364.789,195.917,316.082,147.211,256,147.211z"/>
									</g>
									<g>
										<circle style="fill:#495F7A;" cx="256" cy="256" r="58.134"/>
									</g>
									<g>
										<path style="fill:#385068;" d="M256,197.866c-0.125,0-0.247,0.009-0.371,0.009v116.248c0.124,0.001,0.247,0.009,0.371,0.009    c32.106,0,58.134-26.027,58.134-58.134S288.106,197.866,256,197.866z"/>
									</g>
								</g>
							</svg>
						</span>
						<?php 
							$print_views = empty($views) ? 0 : $views;
							echo "<span>{$print_views}</span>";
						?>
					</span>
					<div class="thumbnail_cnt__inner">
						<a href="<?php the_permalink(); ?>" title="Link to <?php the_title_attribute(); ?>" class="thumbnail_cnt__link">
							<?php 
								the_post_thumbnail('game-thumbnail', 
								$attr = array(
										'class' => "thumbnail",
										'alt' => esc_attr(get_the_title()),
										'sizes' => '(max-width: 480px) 550px, (min-width: 481px) 550px, 550px'
									)
								); ?>
	                    </a> 
					</div>

					<?php if(get_post_type() === 'games'): ?>

					<div class="pad_outer <?php echo (!empty($bage) ? 'pad_outer--offset': ''); ?>">
						<div class="pad">
							<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
									viewBox="0 0 468.293 468.293" style="enable-background:new 0 0 468.293 468.293;" xml:space="preserve" width="25px" height="25px">
								<g>
									<path style="fill:#D5D6DB;" d="M337.881,71.84H108.229c-4.472,0-8.096,3.625-8.096,8.096c0,4.472,3.625,8.096,8.096,8.096h229.652
										c7.769,0,14.086,6.318,14.086,14.081s-6.318,14.081-14.086,14.081h-81.764c-16.582,0-30.07,13.488-30.07,30.07v12.122
										c0,4.472,3.625,8.096,8.096,8.096c4.472,0,8.096-3.625,8.096-8.096v-12.122c0-7.656,6.222-13.878,13.878-13.878h81.764
										c16.695,0,30.279-13.584,30.279-30.273S354.576,71.84,337.881,71.84z"/>
									<path style="fill:#D5D6DB;" d="M358.446,396.453h-248.6C49.18,396.453,0,347.273,0,286.607v0C0,225.94,49.18,176.76,109.846,176.76
										h248.6c60.666,0,109.846,49.18,109.846,109.846v0C468.293,347.273,419.113,396.453,358.446,396.453z"/>
								</g>
								<g>
									<circle style="fill:#EBF0F3;" cx="360.086" cy="286.595" r="85.104"/>
									<circle style="fill:#EBF0F3;" cx="108.232" cy="286.595" r="85.104"/>
								</g>
								<polygon style="fill:#27A2DB;" points="166.045,271.327 123.51,271.327 123.51,228.793 92.951,228.793 92.951,271.327 
									50.417,271.327 50.417,301.886 92.951,301.886 92.951,344.421 123.51,344.421 123.51,301.886 166.045,301.886 "/>
								<g>
									<circle style="fill:#44C4A1;" cx="360.086" cy="245.129" r="20.236"/>
									<circle style="fill:#44C4A1;" cx="360.086" cy="328.055" r="20.236"/>
								</g>
								<g>
									<circle style="fill:#1C75A1;" cx="401.539" cy="286.595" r="20.236"/>
									<circle style="fill:#1C75A1;" cx="318.564" cy="286.595" r="20.236"/>
								</g>
								<path style="fill:#3A556A;" d="M263.053,176.76h-57.814v-12.133c0-3.448,2.796-6.244,6.244-6.244h45.326
									c3.448,0,6.244,2.796,6.244,6.244V176.76z"/>
							</svg>
						</div>
						<?php
							$game_types = get_the_terms( get_the_ID(), 'game_types' );

							if(!empty($game_types)):

								foreach( $game_types as $type ) {
									
									if($type->slug == 'battlefield'){
										$game_type_icon = sprintf(

									        '<div class="pad pad--type">
									        	<a href="%1$s" title="%2$s">
													<svg xmlns="http://www.w3.org/2000/svg" height="22px" viewBox="0 0 463.01334 463.01334" width="22px"><path d="m455.019531 7.992188-33.53125 95.101562c-5.992187 16.992188-15.714843 32.425781-28.457031 45.167969l-11.210938 11.210937-78.28125-78.28125 11.210938-11.207031c12.742188-12.742187 28.175781-22.46875 45.167969-28.460937zm0 0" fill="#fedb41"/><path d="m381.820312 159.472656-49.652343 49.640625-.96875.96875-78.269531-78.269531.96875-.96875 49.640624-49.652344zm0 0" fill="#f4b844"/><path d="m337.238281 216.121094c6.980469 6.996094 6.980469 18.320312 0 25.3125l-139.207031 139.199218-31.542969 31.539063-115.648437-115.648437 31.539062-31.539063 139.199219-139.210937c6.996094-6.980469 18.320313-6.980469 25.3125 0zm0 0" fill="#00acea"/><path d="m166.488281 412.171875-42.828125 42.839844-115.660156-115.660157 42.839844-42.828124zm0 0" fill="#00efd1"/><path d="m452.359375.449219-95.097656 33.527343c-18.113281 6.414063-34.566407 16.78125-48.167969 30.347657l-56.164062 56.179687-.386719-.386718c-10.125-10.085938-26.496094-10.085938-36.621094 0l-139.199219 139.207031-74.378906 74.371093c-3.125 3.125-3.125 8.191407 0 11.3125l115.660156 115.660157c3.125 3.125 8.191406 3.125 11.316406 0l42.828126-42.835938 170.753906-170.746093c10.082031-10.125 10.082031-26.496094 0-36.621094l-.390625-.382813 56.171875-56.164062c13.566406-13.601563 23.933594-30.054688 30.347656-48.164063l33.53125-95.101562c1.019531-2.902344.285156-6.132813-1.890625-8.308594-2.175781-2.175781-5.40625-2.910156-8.3125-1.890625zm-328.699219 443.253906-104.351562-104.347656 31.53125-31.519531 104.339844 104.339843zm207.921875-207.925781-139.210937 139.199218-25.878906 25.882813-104.339844-104.335937 165.082031-165.089844c3.871094-3.851563 10.128906-3.851563 14 0l90.34375 90.347656c3.851563 3.867188 3.855469 10.121094.003906 13.996094zm-67.339843-103.964844 39.300781-39.304688 66.96875 66.960938-39.308594 39.300781zm149.699218-31.378906c-5.613281 15.855468-14.691406 30.257812-26.570312 42.167968l-5.550782 5.554688-66.964843-66.964844 5.550781-5.554687c11.910156-11.878907 26.316406-20.953125 42.171875-26.570313l79.339844-27.972656zm0 0" fill="#083863"/></svg>
									        	</a>
									        	</div>',
									        esc_url( get_category_link( $type->term_id ) ),
									        esc_attr( sprintf( __( 'View all games in %s category', 'adventurebeta2_theme' ), $type->name ) ),
									        esc_html__($type->count),
									        esc_html( $type->name )
									    );
			 
										echo $game_type_icon; 
									}

									if($type->slug == 'open-world'){
										$game_type_icon = sprintf(

									        '<div class="pad pad--type">
									        	<a href="%1$s" title="%2$s">
													<svg xmlns="http://www.w3.org/2000/svg" height="25px" viewBox="-48 0 496 496" width="25px"><path d="m380.953125 77.960938 11.050781 10.039062-6.730468 55.535156c-16.246094 134.03125-82.398438 257.023438-185.269532 344.464844-102.875-87.441406-169.027344-210.433594-185.273437-344.464844l-6.726563-55.535156 11.046875-10.039062c49.511719-45.015626 114.03125-69.960938 180.953125-69.960938 66.917969 0 131.4375 24.945312 180.949219 69.960938zm0 0" fill="#f0d365"/><path d="m272.003906 359.105469c36.589844-53.554688 60.703125-115.195313 69.695313-180.410157l10.304687-74.695312c-23.480468-20.542969-50.753906-35.824219-80-45.191406zm0 0" fill="#976947"/><path d="m197.042969 48c-23.625 0-46.898438 3.710938-69.039063 10.808594v300.289062c20.300782 29.710938 44.390625 56.972656 72 80.894532v-391.992188zm0 0" fill="#976947"/><path d="m128.003906 359.105469v-300.296875c-29.25 9.367187-56.523437 24.648437-80 45.191406l10.300782 74.695312c8.992187 65.214844 33.105468 126.847657 69.699218 180.410157zm0 0" fill="#c18e59"/><path d="m272.003906 58.808594c-22.144531-7.097656-45.417968-10.808594-69.042968-10.808594h-2.957032v392c27.605469-23.921875 51.695313-51.183594 72-80.894531zm0 0" fill="#c18e59"/><path d="m232.003906 136c0 17.671875-14.328125 32-32 32-17.675781 0-32-14.328125-32-32s14.324219-32 32-32c17.671875 0 32 14.328125 32 32zm0 0" fill="#8bd2f5"/><path d="m200.003906 496c-1.839844 0-3.691406-.640625-5.175781-1.902344-104.722656-89.007812-171.496094-213.167968-188.035156-349.601562l-6.7343752-55.535156c-.3124998-2.570313.6406252-5.136719 2.5585942-6.882813l11.050781-10.039063c51.09375-46.453124 117.269531-72.039062 186.335937-72.039062 69.0625 0 135.238282 25.585938 186.335938 72.039062l11.046875 10.039063c1.917969 1.746094 2.871093 4.3125 2.558593 6.882813l-6.726562 55.535156c-16.535156 136.433594-83.3125 260.59375-188.03125 349.601562-1.496094 1.261719-3.34375 1.902344-5.183594 1.902344zm-183.5625-404.855469 6.234375 51.429688c15.773438 130.160156 78.664063 248.808593 177.328125 334.867187 98.664063-86.058594 161.550782-204.707031 177.328125-334.867187l6.230469-51.429688-7.992188-7.265625c-48.144531-43.773437-110.496093-67.878906-175.566406-67.878906-65.074218 0-127.425781 24.105469-175.570312 67.871094zm0 0"/><path d="m200.003906 448c-1.867187 0-3.738281-.65625-5.242187-1.953125-78.847657-68.335937-130.128907-162.894531-144.382813-266.253906l-10.304687-74.695313c-.367188-2.664062.632812-5.335937 2.65625-7.113281 42.734375-37.390625 97.535156-57.984375 154.3125-57.984375h5.910156c56.785156 0 111.585937 20.59375 154.3125 57.984375 2.023437 1.777344 3.023437 4.449219 2.65625 7.113281l-10.304687 74.695313c-14.261719 103.359375-65.535157 197.917969-144.382813 266.253906-1.496094 1.296875-3.367187 1.953125-5.230469 1.953125zm-143.480468-340.769531 9.703124 70.371093c13.414063 97.214844 60.832032 186.335938 133.777344 251.734376 72.941406-65.398438 120.359375-154.519532 133.773438-251.734376l9.703125-70.371093c-39.316407-33.0625-89.046875-51.230469-140.519531-51.230469h-5.917969c-51.472657 0-101.199219 18.167969-140.519531 51.230469zm0 0"/><path d="m192.003906 168h16v272h-16zm0 0"/><path d="m192.003906 48h16v56h-16zm0 0"/><path d="m264.003906 56h16v304h-16zm0 0"/><path d="m120.003906 56h16v304h-16zm0 0"/><path d="m200.003906 176c-22.058594 0-40-17.945312-40-40s17.941406-40 40-40c22.054688 0 40 17.945312 40 40s-17.945312 40-40 40zm0-64c-13.234375 0-24 10.769531-24 24s10.765625 24 24 24c13.230469 0 24-10.769531 24-24s-10.769531-24-24-24zm0 0"/></svg>
									        	</a>
									        	</div>',
									        esc_url( get_category_link( $type->term_id ) ),
									        esc_attr( sprintf( __( 'View all games in %s category', 'adventurebeta2_theme' ), $type->name ) ),
									        esc_html__($type->count),
									        esc_html( $type->name )
									    );
			 
										echo $game_type_icon; 
									}

									if($type->slug == 'other'){
										$game_type_icon = sprintf(

									        '<div class="pad pad--type">
									        	<a href="%1$s" title="%2$s">
												<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="30px" height="30px"
															viewBox="0 0 331.3 176.7" style="enable-background:new 0 0 331.3 176.7;" xml:space="preserve">
													<style type="text/css">
														.st0{fill:#07b0cc;}
														.st1{fill:none;}
														.st2{fill:#FFFFFF;}
														.st3{fill:#b5e2ff;}
													</style>
													<g>
														<circle class="st0" cx="259.7" cy="87.7" r="59.7"/>
														<ellipse class="st1" cx="246.9" cy="87.7" rx="46.9" ry="59.7"/>
														<path class="st2" d="M259.7,147.4c2.4,0,4.8-0.2,7.2-0.5c-23.3-3.5-41.3-28.7-41.3-59.3c0-30.6,18-55.7,41.3-59.3
															c-22-2.7-43.7,7.1-56.3,25.4s-14,42-3.7,61.7C217.1,135.1,237.5,147.4,259.7,147.4L259.7,147.4z"/>
														<circle class="st0" cx="71.9" cy="87.7" r="59.7"/>
														<ellipse class="st1" cx="59.1" cy="87.7" rx="46.9" ry="59.7"/>
														<path class="st2" d="M71.9,147.4c2.4,0,4.8-0.2,7.2-0.5c-23.3-3.5-41.3-28.7-41.3-59.3c0-30.6,18-55.7,41.3-59.3
															c-22-2.7-43.7,7.1-56.3,25.4s-14,42-3.7,61.7C29.4,135.1,49.7,147.4,71.9,147.4L71.9,147.4z"/>
														<g>
															<path class="st3" d="M71.9,156c-37.7,0-68.3-30.6-68.3-68.3s30.6-68.3,68.3-68.3s68.3,30.6,68.3,68.3
																C140.2,125.4,109.6,155.9,71.9,156L71.9,156z M71.9,36.5c-28.3,0-51.2,22.9-51.2,51.2s22.9,51.2,51.2,51.2s51.2-22.9,51.2-51.2
																C123.1,59.4,100.2,36.5,71.9,36.5L71.9,36.5z"/>
															<path class="st3" d="M259.7,156c-37.7,0-68.3-30.6-68.3-68.3s30.6-68.3,68.3-68.3s68.3,30.6,68.3,68.3
																C327.9,125.4,297.4,155.9,259.7,156L259.7,156z M259.7,36.5c-28.3,0-51.2,22.9-51.2,51.2s22.9,51.2,51.2,51.2
																c28.3,0,51.2-22.9,51.2-51.2C310.8,59.4,287.9,36.5,259.7,36.5L259.7,36.5z"/>
														</g>
													</g>
													</svg>
									        	</a>
									        	</div>',
									        esc_url( get_category_link( $type->term_id ) ),
									        esc_attr( sprintf( __( 'View all games in %s category', 'adventurebeta2_theme' ), $type->name ) ),
									        esc_html__($type->count),
									        esc_html( $type->name )
									    );
			 
										echo $game_type_icon; 
									}
								     
								} 
							endif;
						?>
						
					</div>
					<?php endif; ?>
					<div class="game_info">	
						<div class="popover">
							<div class="card">
								<h5 class="card__header">
									<a href="<?php the_permalink() ?>" title="Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a>
								</h5>
							</div>
						</div>
					</div>		
				</figure> 			

			<?php endif;?>

	</div>
</article>