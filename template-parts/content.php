<?php 
	$iframe_link = get_post_meta($post->ID, '_voxel_games_iframe_link_value_key', true);
?>
<article <?php post_class( array( 'single_post' ) ); ?>>

	<div>
		<header class="to_animate">
			<h1><?php the_title(); ?></h1>
		</header>
		<div class="post_info to_animate">
			<div class="categories">
				<?php
					$categories = get_the_category();
                    $divider = ', ';
					if ( ! empty( $categories ) ) {
						echo 'Category: ';
						foreach ($categories as $key => $cat) {
							if(count($categories) != ($key + 1 )){
								echo '<a href="' . esc_url( get_category_link( $cat->term_id ) ) . '">' . esc_html( $cat->name ) . '</a>'. $divider; 
							}else{
								echo '<a href="' . esc_url( get_category_link( $cat->term_id ) ) . '">' . esc_html( $cat->name ) . '</a>'; 
							}
						} 
					} 
				 ?>
			</div>
		</div>
		<div>
			<?php the_content(); ?>
		</div>
	</div>
	
</article>