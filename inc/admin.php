<?php 
// login logo on wp-admin page
	function voxel_login_logo() { ?>
	    <style type="text/css">
	        #login h1 a, .login h1 a {
	            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/logo_a.png);
	            background-size: contain;
	        }
	    </style>
	<?php }
	add_action( 'login_enqueue_scripts', 'voxel_login_logo' );
//logo link on wp admin page
	function voxel_login_logo_url() {
	    return home_url();
	}
	add_filter( 'login_headerurl', 'voxel_login_logo_url' );