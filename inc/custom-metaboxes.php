<?php
	//create game views meta field in db start
	function create_game_views_meta_key($post_id){
		if ( !metadata_exists( 'post', $post_id, 'voxel_theme_game_views' ) ) {
		    $metaKey = 'voxel_theme_game_views';
		    add_post_meta( $post_id, $metaKey, 0, true );
		}
	}	
	//create game views meta field in db end
	function voxel_theme_add_meta_box() {
		add_meta_box( 'games_iframe_link', __('Iframe link'), 'games_iframe_link_callback', 'games' );
	}
	function games_iframe_link_callback($post){
		wp_nonce_field( 'voxel_save_iframe_link_data', 'games_iframe_link_box_nonce' );

		$value = get_post_meta( $post->ID, '_voxel_games_iframe_link_value_key', true );
		echo '<input id="games_iframe_link_field" type="url" name="games_iframe_link_field" size="60" value="' . esc_attr( $value ) . '">';
	}

	function voxel_save_iframe_link_data($post_id){
		if( ! isset( $_POST['games_iframe_link_box_nonce'] ) ){
			return;
		}
		
		if( ! wp_verify_nonce( $_POST['games_iframe_link_box_nonce'], 'voxel_save_iframe_link_data') ) {
			return;
		}
		
		if( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ){
			return;
		}
		
		if( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}
		
		if( ! isset( $_POST['games_iframe_link_field'] ) ) {
			return;
		}

		$my_data = sanitize_text_field( $_POST['games_iframe_link_field'] );
		create_game_views_meta_key($post_id);
		update_post_meta( $post_id, '_voxel_games_iframe_link_value_key', $my_data );
	}

	add_action( 'add_meta_boxes', 'voxel_theme_add_meta_box' );
	add_action( 'save_post', 'voxel_save_iframe_link_data' );